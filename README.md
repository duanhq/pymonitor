#pymonitor
pymonitor是以python编写的资源监控软件；monitor_client是客户端，用在被监控的主机上；monitor_server是服务端，用在监控主机上。

#monitor_server使用
##配置文件
需要监控的主机及其监控项皆在文件/monitor_server/conf/hosts.py中登记
1.监控项设置样式
监控项设置是在监控服务器上配置的，由客户端连接服务器读取分配给自己的设置并按之工作
```
h1 = templates.LinuxGeneralServices()  #创建被监控主机的实例
h1.services['cpu'].interval = 15  # 创建cpu资源监控的周期（单位秒）
h1.osname = 'ubuntu15.10'  # 客户机的系统版本
h1.hostname = 'oschina'  # 主机名称
h1.ipaddress = '192.168.1.1' # 主机的IP地址
h1.port = 22  # 主机SSH端口（用于发送数据到监控机）
h1.services['cpu'].triggers['iowait'] = ['percentage', 30, 50]  # 定制CPU的iowait命令结果的阀值，并注明是百分比
```
2.待补充
#monitor_client使用
待补充
