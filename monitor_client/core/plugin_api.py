#!/usr/bin/env python
#condint:UTF-8

from core import global_setting
from plugins import upCheck, cpu, load, memory

def upCheck_info():
    return upCheck.monitor()

def cpu_info():
    data = cpu.monitor()
    print data

def load_info():
    data = load.monitor()
    print data
    return data

def mem_info():
    return memory.monitor()

"""
def sys_info():
    return sys_Info.monitor()

def cpu_info():
    return cpu.monitor()

def mem_info():
    return memory.monitor()

def disk_info():
    return disk.monitor()

def net_info():
    return netinfo.monitor()
"""