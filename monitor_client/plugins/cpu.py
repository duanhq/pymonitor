#!/usr/bin/env python
#coding:UTF-8
import commands


def monitor(frist_invoke=1):
    'use shell command sar to take status of cpus'
    shell_command = 'sar 1 3 | grep "平均时间:"'
    # if sys_lang = english, to grep "^Average:"
    status, result = commands.getstatusoutput(shell_command)
    if status != 0:
        value_dic = {'status': status}
    else:
        user, nice, system, iowait, steal, idle = result.split()[2:]
        value_dic = {
            'user': user,
            'nice': nice,
            'system': system,
            'iowait': iowait,
            'steal': steal,
            'idle': idle,
            'status': status
        }
    return value_dic

if __name__ == '__main__':
    print monitor()
