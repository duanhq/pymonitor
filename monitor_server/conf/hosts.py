#!/usr/bin/env python
# -*- coding:utf-8 -*-

from conf import templates


# host example
'''
h1 = templates.LinuxGeneralServices()
h1.services['cpu'].interval = 15
h1.osname = 'gentoo6.0'
h1.hostname = 'alex_test'
h1.ipaddress = '192.168.2.3'
h1.port = 22
h1.services['cpu'].triggers['iowait'] = ['percentage', 30, 50]
'''

monitored_hosts = (
    #example,
)


if __name__ == '__main__':
    for h in monitored_hosts:
        print('hostname::', h.hostname)
        print('services::', h.services.keys())
        print('>' * 20)

