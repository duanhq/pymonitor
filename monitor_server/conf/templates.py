#!/usr/bin/env python

from conf.services import linux, generic


class BaseTemplate:
    osname = None
    alert_policy = None
    groups = None
    hostname = None
    ipaddress = None
    port = None
    services = None


class LinuxGeneralServices(BaseTemplate):
    def __init__(self):
        self.osname = 'Linux General Services'
        self.groups = ['TestGroup', ]
        # self.hosts = ['localhost','www.baidu.com']
        self.services = {
            'cpu': linux.cpu(),
            'memory':  linux.memory(),
            'load':  linux.load(),
            'upCheck': generic.upCheck(),
        }


class WindowsGeneralService(BaseTemplate):
    def __init__(self):
        self.osname = 'Windows General Services'
        # self.groups = ['BJ']
        # self.hostname = ['localhost','www.baidu.com']
        self.services = {
            'load': linux.load(),
            'memory':  linux.memory(),
            'cpu': linux.cpu(),
            }

enabled_templates = (
    LinuxGeneralServices(),
    WindowsGeneralService()
    # TestBaseTemplate(),
)
