#!/usr/bin/env python
# -*- coding: utf-8 -*-


class DefaultService:
    name = 'DefaultService'
    interval = 300
    plugin_name = None
    index_dic = None
    data_from = 'agent'
    failurecount = 5
    failuretime = 660
    graph_index = {
        'index': [],
        'title': name,
        }
    lt_operator = []  # if this sets to empty,all the status will be \
    # caculated in > mode , gt = >


class upCheck(DefaultService):
    def __init__(self):
        self.name = 'host status'
        self.interval = 30
        self.plugin_name = 'upCheck_info'
        self.failurecount = 5
        self.failuretime = 330
        self.triggers = {
            'host_status': [None, 0 , 90],
            }