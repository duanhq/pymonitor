#!/usr/bin/env python
# -*- coding: utf-8 -*-

from generic import DefaultService


class ngnix(DefaultService):
    def __init__(self):
        self.name = "Ngnix status"
        self.interval = 30
        self.failurecount = 5
        self.failuretime = 330
        self.triggers = {
            'alive': ['int', 1, 10]
        }


class cpu(DefaultService):
    def __init__(self):
        self.name = 'cpu'
        self.interval = 30
        self.plugin_name = 'cpu_info'
        self.failurecount = 5
        self.failuretime = 330
        self.triggers = {
            'iowait': ['percentage', 5.5, 90],
            'system': ['percentage', 5, 90],
            'idle': ['percentage', 20, 10],
            'user': ['percentage', 80, 90],
            'steal': ['percentage', 80, 90],
            'nice': [None, 80, 90],
            }
        self.graph_index = {
            'index': ['iowait', 'system', 'idle', 'user'],
            'title': 'CPU usage',
            }
        self.lt_operator = ['idle']


class load(DefaultService):
    def __init__(self):
        self.name = 'load'
        self.interval = 300
        self.plugin_name = 'load_info'
        self.failurecount = 5
        self.failuretime = 3300
        self.triggers = {
            'uptime': ['string', 0, 90],
            # 'ptime': ['string', 'd', 90],
            'load1': ['int', 4, 9],
            'load5': ['int', 3, 7],
            'load15': ['int', 3, 9],
            }
        self.graph_index = {
            'index': ['load1', 'load5', 'load15'],
            'title': 'Load status',
            }


class memory(DefaultService):
    def __init__(self):
        self.name = 'memory'
        self.interval = 100
        self.plugin_name = 'mem_info'
        self.failurecount = 5
        self.failuretime = 1100
        self.triggers = {
            'SwapUsage_p': ['percentage', 66, 91],
            'MemUsage_p': ['percentage', 68, 92],
            #'MemUsage': [None, 60, 65],
        }
        self.graph_index = {
            'index': ['MemUsage', 'SwapUsage'],

        }
